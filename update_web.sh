#!/bin/bash
HEAD_COMMIT=`git rev-parse --short HEAD`
# Run the command to check if the repo has uncommited changes.
git diff-index --quiet HEAD --
if [ $? -eq 1 ]
then
  #Error means dirty
  HEAD_COMMIT="${HEAD_COMMIT}_dirty"
fi
echo ${HEAD_COMMIT}

npm run build
rm -rf ../ademille.bitbucket.io/tictactoe
cp -r build/ ../ademille.bitbucket.io/tictactoe
cd ../ademille.bitbucket.io
git add tictactoe/
git commit -am "Updated Tic Tac Toe to ${HEAD_COMMIT}"
git push origin

